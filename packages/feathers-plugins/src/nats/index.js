/* eslint-disable no-console */
/* eslint-disable no-use-before-define */
const nats = require("nats");
const debug = require("debug")("nats");
const { checkExpected } = require("@lpgroup/feathers-utils");
const { natsYupError } = require("./natsYupError");

/**
 * Start a nats server used by microservices
 *
 * "nats": {
 *   "name": "lpgroup-ng",
 *   "uri": "nats://localhost:4222",
 *   "queue": "file-ng-dev",
 *   "messagePrefix": "dev",
 *   "versions": {
 *     "post.document.transaction-forms.request": "v1",
 *     "post.document.transaction-forms.response": "v1",
 *   }
 * },
 */

async function init(app) {
  return initWithOptions(app.get("nats"));
}

async function initWithOptions(options) {
  return createNatsServer(options);
}

async function createNatsServer(options) {
  const { queue } = options;
  const nc = nats.connect({
    name: `${options.name} ${options.messagePrefix}`,
    uri: options.uri,
    reconnect: true,
    maxReconnectAttempts: -1,
    reconnectTimeWait: 2000,
    // TODO: ,json: true
  });

  nc.on("connect", () => {
    // Do something with the connection
    console.log(`Connected to NATS server: ${options.uri} and queue ${options.queue}`);
  });

  nc.on("disconnect", () => {
    console.log(`disconnect ${options.uri} ${options.queue}`);
  });

  nc.on("reconnecting", () => {
    console.log(`reconnecting ${options.uri} ${options.queue}`);
  });

  nc.on("reconnect", () => {
    console.log(`reconnect ${options.uri} ${options.queue}`);
  });

  nc.on("close", () => {
    console.log(`close ${options.uri} ${options.queue}`);
  });

  nc.on("error", (err) => {
    console.error("error", err, options);
  });

  nc.closePlugin = () => {
    nc.close();
  };

  // TODO:
  // eslint-disable-next-line no-unused-vars
  nc.waitOnServerReady = async (url) => {};

  nc.sub = (subject, service) => {
    const subscribeSubject = getSubscribeSubject(options, subject);
    console.log(`NATS Subscribe on ${subscribeSubject}`);

    nc.subscribe(subscribeSubject, { queue }, (msg, reply) => {
      try {
        const request = JSON.parse(msg);
        debug(`Incoming on "${subscribeSubject}" ${reply || ""}`);
        validateYup(request, service.options.schema.request)
          .then((v) => {
            service.request(v, reply).catch((err) => {
              service.error(request, err, reply);
            });
          })
          .catch((err) => {
            service.error(request, err, reply);
          });
      } catch (err) {
        debug(`Incoming error on "${subscribeSubject}" ${reply || ""} ${err.message}`);
        service.error({}, err, reply);
      }
    });
  };

  nc.pub = async (subject, msg) => {
    const subscribeSubject = getSubscribeSubject(options, subject);
    debug(`Publish on "${subscribeSubject}"`);
    const response = JSON.stringify(msg);
    return nc.publish(subscribeSubject, response);
  };

  // Store subscribtion promises from subscribe()
  // incomming() can then wait for them to finish.
  nc.subscribes = {};

  nc.subExpect = (subject, configOptions = {}) => {
    const subscribeSubject = getSubscribeSubject(options, subject);
    const promise = new Promise((resolve, reject) => {
      nc.subscribe(subscribeSubject, (msg, reply) => {
        debug(`Incoming on "${subscribeSubject}" ${reply || ""}`);
        const request = JSON.parse(msg);

        if (
          !checkExpected(request, { ignoreKeyCompare: options.ignoreKeyCompare, configOptions })
        ) {
          resolve(request);
        } else {
          reject(request);
        }
      });
    });
    nc.subscribes[subscribeSubject] = nc.subscribes[subscribeSubject]
      ? nc.subscribes[subscribeSubject].push(promise)
      : [promise];
  };

  nc.waitForSubExpect = async (subject) => {
    const subscribeSubject = getSubscribeSubject(options, subject);
    debug(`Wait for incoming: ${subscribeSubject}`);
    if (nc.subscribes[subscribeSubject] && nc.subscribes[subscribeSubject].length > 0) {
      const result = await Promise.all(nc.subscribes[subscribeSubject]).catch(() => {
        debug(`Failed incoming: ${subscribeSubject}`);
      });
      delete nc.subscribes[subscribeSubject];
      return result;
    }
    debug(`No subscription added for: ${subscribeSubject}`);
    return undefined;
  };

  return nc;
}

/**
 * Adding int.v2 in the subject "int.v2.post.votes.cats.request"
 */
function getSubscribeSubject(configOptions, subject) {
  const prefix = configOptions.messagePrefix || "NA";
  const version = configOptions.versions && configOptions.versions[subject];
  if (version) return `${prefix}.${version}.${subject}`;
  return `${prefix}.${subject}`;
}

function validateYup(data, validateSchema) {
  return validateSchema.validate(data, { strict: false, abortEarly: false, stripUnknown: false });
}

module.exports = { init, initWithOptions, createNatsServer, natsYupError };
