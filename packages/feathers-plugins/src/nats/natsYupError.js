function natsYupError(msg, yupError) {
  const data = {
    referenceId: msg.referenceId || "",
    status: "FAIL",
    message: yupError.message,
    code: 400,
    data: {},
    errors: [],
  };
  if (yupError.inner) {
    data.errors = yupError.inner.map((val) => {
      return {
        message: val.message,
        path: val.path,
        value: val.value,
        type: val.type,
      };
    });
  }
  return [data];
}

module.exports = { natsYupError };
