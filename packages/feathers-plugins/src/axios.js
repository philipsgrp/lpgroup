const { setupAxios, instance } = require("@lpgroup/feathers-utils");

async function initWithOptions(options) {
  setupAxios(options);
  return instance();
}

async function init(app, options) {
  return initWithOptions(options);
}

module.exports = { init, initWithOptions };
