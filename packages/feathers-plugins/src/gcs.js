/* eslint-disable no-console */
/* eslint-disable no-use-before-define */
// Setup Google Cloud Storage plugin

const { Storage } = require("@google-cloud/storage");
const { parseDataURI } = require("dauria");
const debug = require("debug")("gcs");

let storage = null;
let bucket = null;

async function init(app) {
  return initWithOptions({
    imageGcsProject: app.get("google").imageGcsProject,
    imageGcsCredentials: app.get("google").imageGcsCredentials,
    imageGcsBucket: app.get("google").imageGcsBucket,
  });
}

async function initWithOptions(options) {
  if (!storage) {
    await setupCloudStorage(
      options.imageGcsProject,
      options.imageGcsCredentials,
      options.imageGcsBucket
    );
  }
  return {
    storage,
    bucket,
    closePlugin,
    exists,
    uploadFile,
    downloadFile,
    removeFile,
    listFiles,
  };
}

async function setupCloudStorage(projectId, keyFilename, bucketName) {
  storage = new Storage({ projectId, keyFilename });
  bucket = storage.bucket(bucketName);
  return verifyBucket(bucketName);
}

/**
 * Verify that bucket exist on google cloud storage.
 */
async function verifyBucket(bucketName) {
  return bucket
    .getFiles({ prefix: "don't like to find anything" })
    .then(() => {
      console.log(`Connected to GCP bucket: ${bucketName}`);
    })
    .catch((err) => {
      console.error(`Can't connect to gcp bucket: ${bucketName}`, err);
      throw Error(`Can't connect to gcp bucket: ${bucketName}`);
    });
}

async function closePlugin() {
  // TODO: Does this exist?
  // bucket.close();
  // storage.close();
}

async function exists(fileName) {
  const file = bucket.file(fileName);
  const result = await file.exists();
  return result[0];
}

async function uploadFile(fileName, uri, meta = {}) {
  debug(`Upload to gcs ${fileName}`);
  const result = parseDataURI(uri);
  const file = bucket.file(fileName);
  await file
    .save(result.buffer, { resumable: false })
    .catch((err) => console.error(`Cant save file to cloud storage: ${fileName}`, err));
  await file.setMetadata({
    metadata: {
      ...meta,
      // Enable long-lived HTTP caching headers
      // Use only if the contents of the file will never change
      // (If the contents will change, use cacheControl: 'no-cache')
      cacheControl: "public, max-age=600",
    },
  });

  return {
    src: file.metadata.name,
    contentType: file.metadata.contentType,
    size: file.metadata.size,
  };
}

async function downloadFile(fileName) {
  const file = bucket.file(fileName);
  return file
    .download()
    .then((v) => {
      return v[0];
    })
    .catch((err) => {
      const message = `Can't download file from gcs: ${fileName} ${err.code} ${err.response.statusMessage}`;
      console.error(message);
      throw Error(message);
    });
}

async function removeFile(fileName) {
  const [files] = await bucket.getFiles({ prefix: fileName });
  const result = files.map(async (file) => {
    await file.delete().catch((err) => {
      console.debug(`Error deleting file from gcs: ${err}`);
    });
    return { src: file.metadata.name };
  });
  return Promise.all(result);
}

async function listFiles(prefix) {
  const [files] = await bucket.getFiles({ prefix });
  return files.map((x) => ({
    name: x.name,
    md5Checksum: x.metadata.metadata.gdriveMD5,
    mediaLink: x.metadata.mediaLink,
    size: x.metadata.size,
  }));
}

module.exports = { init, initWithOptions };
