# example

An example app using @lpgroup/feathers-auth-service

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

    npm install
    npm start

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.
