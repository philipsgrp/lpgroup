// TODO: Need checkPermission hook

const { authenticate } = require("@feathersjs/authentication").hooks;

const { hashPassword } = require("@feathersjs/authentication-local").hooks;
const { discard, iff, isProvider } = require("feathers-hooks-common");

module.exports = {
  before: {
    all: [],
    find: [authenticate("jwt")],
    get: [authenticate("jwt")],
    create: [hashPassword("password")],
    update: [hashPassword("password"), authenticate("jwt")],
    patch: [hashPassword("password"), authenticate("jwt")],
    remove: [authenticate("jwt")],
  },

  after: {
    all: [ iff(isProvider("external"), discard("password")) ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
