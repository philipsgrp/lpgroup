const { internalParams } = require("@lpgroup/feathers-utils");

class UsersUpgrade {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async create(data, params) {
    const { userId } = params.query;
    const userData = {
      ...data,
      _id: userId,
      type: "user",
      privileges: [{ alias: "standard_user", params: { userId } }],
    };
    return this.app
      .service("users")
      .patch(userId, userData, internalParams({ ...params, superUser: true }));
  }
}

module.exports = { UsersUpgrade };
