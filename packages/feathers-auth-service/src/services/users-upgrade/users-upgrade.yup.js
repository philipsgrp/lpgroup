const cy = require("@lpgroup/yup");

const requestSchema = {
  email: cy.email().required(),
  password: cy.string().required(),
  firstName: cy.string().defaultNull(),
  lastName: cy.string().defaultNull(),
  phone: cy.phone().defaultNull(),
};

const dbSchema = {};
module.exports = cy.buildValidationSchema(requestSchema, dbSchema);
