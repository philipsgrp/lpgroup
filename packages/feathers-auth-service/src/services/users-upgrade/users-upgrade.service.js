const { UsersUpgrade } = require("./users-upgrade.class");
const hooks = require("./users-upgrade.hooks");
const schema = require("./users-upgrade.yup");

module.exports = (app) => {
  const options = {
    id: "_id",
    schema,
  };

  app.use("/users/:userId/upgrade", new UsersUpgrade(options, app));
  const service = app.service("users/:userId/upgrade");
  service.hooks(hooks);
};

module.exports.UsersUpgrade = UsersUpgrade;
