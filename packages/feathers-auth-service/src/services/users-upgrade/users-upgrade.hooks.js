const { disallow } = require("feathers-hooks-common");
const { discard, iff, isProvider } = require("feathers-hooks-common");
const { validReq } = require("@lpgroup/yup");

const auth = require("../../hooks/auth");

module.exports = {
  before: {
    all: [auth()],
    find: [disallow()],
    get: [disallow()],
    create: [validReq()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    // Make sure the password field is never sent to the client
    // Always must be the last hook
    all: [
      iff(isProvider("external"), discard("password", "privileges", "signInCode", "signInEnabled")),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
