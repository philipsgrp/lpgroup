const { Service } = require("feathers-mongodb");
const { onPluginReady } = require("@lpgroup/feathers-plugins");

class Privileges extends Service {
  constructor(options) {
    super(options);

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("privileges");
      database.createIndexThrowError("privileges", { alias: 1 }, { unique: true });
    });
  }
}

module.exports = { Privileges };
