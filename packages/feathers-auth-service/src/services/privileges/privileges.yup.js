const cy = require("@lpgroup/yup");

const requestSchema = {
  _id: cy.id(),
  alias: cy.alias().required(),
  name: cy.string().required(),
  permissions: cy.lazyObject({
    query: cy.arrayObject({
      key: cy.string().required(),
      param: cy.string().required(),
    }),
    methods: cy.array(cy.string()),
  }),
};

const dbSchema = {
  added: cy.changed(),
  changed: cy.changed(),
  owner: cy.userOwner(),
};

module.exports = cy.buildValidationSchema(requestSchema, dbSchema);
