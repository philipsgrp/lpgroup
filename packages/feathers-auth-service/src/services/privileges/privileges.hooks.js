const { iff, isProvider } = require("feathers-hooks-common");
const { patchData, loadData } = require("@lpgroup/feathers-mongodb-hooks").hooks;
const { validateRequest, validateDatabase } = require("@lpgroup/yup");
const { url } = require("@lpgroup/feathers-utils").hooks;
const changed = require("../../hooks/changed");
const auth = require("../../hooks/auth");

module.exports = {
  before: {
    all: [iff(isProvider("external"), auth())],
    find: [],
    get: [],
    create: [validateRequest(), changed(), validateDatabase()],
    update: [validateRequest(), loadData(), changed(), validateDatabase()],
    patch: [validateRequest(), patchData(), changed(), validateDatabase()],
    remove: [],
  },

  after: {
    all: [url({ key: "_id" })],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
