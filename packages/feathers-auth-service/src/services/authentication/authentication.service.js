const { AuthenticationService, JWTStrategy } = require("@feathersjs/authentication");
const { expressOauth } = require("@feathersjs/authentication-oauth");
const hooks = require("./authentication.hooks");
const { LocalStrategy } = require("./local-strategy");
const { DeviceStrategy } = require("./device-strategy");

module.exports = (app) => {
  const authentication = new AuthenticationService(app);

  authentication.register("jwt", new JWTStrategy());
  authentication.register("local", new LocalStrategy());
  authentication.register("device", new DeviceStrategy());

  app.use("/authentication", authentication);
  app.configure(expressOauth());

  // Get our initialized service so that we can register hooks
  const service = app.service("authentication");

  service.hooks(hooks);
};
