const { LocalStrategy: FJLocalStrategy } = require("@feathersjs/authentication-local");

class LocalStrategy extends FJLocalStrategy {
  // eslint-disable-next-line class-methods-use-this, no-unused-vars
  async getEntityQuery(query, params) {
    // Query for user but only include users marked as `active`
    return {
      ...query,
      active: true,
      $limit: 1,
    };
  }
}

module.exports = { LocalStrategy };
