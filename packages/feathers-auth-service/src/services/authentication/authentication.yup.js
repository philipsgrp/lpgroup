const cy = require("@lpgroup/yup");

const requestSchema = {
  strategy: cy
    .string()
    .matches(/(jwt|local|device)/)
    .lowercase()
    .default("local"),
  accessToken: cy.string(), // TODO: Är den verkligen använd?
  _id: cy.uuid().defaultNull(),
  email: cy.email().defaultNull(),
  password: cy.string().defaultNull(),
};

module.exports = cy.buildValidationSchema(requestSchema);
