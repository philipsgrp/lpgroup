const { disallow, discard, iff, isProvider } = require("feathers-hooks-common");
const { validateRequest } = require("@lpgroup/yup");
const { disableSync } = require("@lpgroup/feathers-utils").hooks;
const clearProvider = require("../../hooks/clear-provider");
const schema = require("./authentication.yup");

module.exports = {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [clearProvider(), validateRequest({ schema })],
    update: [disallow()],
    patch: [disallow()],
    remove: [],
  },

  after: {
    all: [
      disableSync(),
      iff(
        isProvider("external"),
        discard(
          "owner",
          "changed",
          "added",
          "signInEnabled",
          "signInCode",
          "active",
          "privileges",
          "password",
          "url"
        )
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
