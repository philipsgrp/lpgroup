const errors = require("@feathersjs/errors");
const { internalParams } = require("@lpgroup/feathers-utils");

/* eslint-disable class-methods-use-this */
const { AuthenticationBaseStrategy } = require("@feathersjs/authentication");

class DeviceStrategy extends AuthenticationBaseStrategy {
  get configuration() {
    const authConfig = this.authentication.configuration;
    const config = super.configuration || {};
    return {
      service: authConfig.service,
      entity: authConfig.entity,
      errorMessage: "Invalid login",
      ...config,
    };
  }

  async findEntity(deviceId, params) {
    const { entityService } = this;
    const { errorMessage } = this.configuration;
    try {
      const result = await entityService.get(
        deviceId,
        internalParams(params, { type: "device", active: true })
      );
      return result;
    } catch (err) {
      throw new errors.NotAuthenticated(errorMessage);
    }
  }

  async authenticate(data, params) {
    const { entity } = this.configuration;
    const deviceId = data._id;
    const result = await this.findEntity(deviceId, internalParams(params));
    return {
      authentication: { strategy: this.name },
      [entity]: result,
    };
  }
}

module.exports = { DeviceStrategy };
