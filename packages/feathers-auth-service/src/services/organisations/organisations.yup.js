const yup = require("@lpgroup/yup");

const requestSchema = {
  _id: yup.id(),
  alias: yup.string().required(),
  name: yup.string().required(),
  organisationNumber: yup.string().defaultNull(),
  email: yup.string().email().defaultNull(),
  phone: yup.phone().defaultNull(),
  website: yup.string().url().defaultNull(),
  contact: yup.emailContact(),
  postAddress: yup.address(),
};

const dbSchema = {
  added: yup.changed(),
  changed: yup.changed(),
  owner: yup.owner(),
};

module.exports = yup.buildValidationSchema(requestSchema, dbSchema);
module.exports.requestSchema = requestSchema;
