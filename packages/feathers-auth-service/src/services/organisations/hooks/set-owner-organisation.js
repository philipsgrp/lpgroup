// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    const { data } = context;
    data.owner.organisation = { alias: data.alias };
    return context;
  };
};
