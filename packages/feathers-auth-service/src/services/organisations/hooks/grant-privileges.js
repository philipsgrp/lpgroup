const { internalParams } = require("@lpgroup/feathers-utils");

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    const { data } = context;

    const userId = context.params.user._id;
    await context.app.service("users").patch(
      userId,
      {
        privileges: [
          {
            alias: "standard_organisation",
            params: { userId, organisationAlias: data.alias },
          },
        ],
      },
      internalParams(context.params)
    );

    return context;
  };
};
