const { patchData, loadData } = require("@lpgroup/feathers-mongodb-hooks").hooks;
const { validateRequest, validateDatabase } = require("@lpgroup/yup");
const { url } = require("@lpgroup/feathers-utils").hooks;
const changed = require("../../hooks/changed");
const auth = require("../../hooks/auth");
const setOwner = require("./hooks/set-owner-organisation");
const grantPrivileges = require("./hooks/grant-privileges");

module.exports = {
  before: {
    all: [],
    find: [auth("public")],
    get: [auth("public")],
    create: [
      auth(),
      validateRequest(),
      changed(),
      setOwner(),
      validateDatabase(),
      grantPrivileges(),
    ],
    update: [auth(), validateRequest(), loadData(), changed(), validateDatabase()],
    patch: [auth(), validateRequest(), patchData(), changed(), validateDatabase()],
    remove: [auth()],
  },

  after: {
    all: [url({ key: "_id" })],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
