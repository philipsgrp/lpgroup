// Initializes the `organisations` service on path `/organisations`
const { mergeValidationsSchema } = require("@lpgroup/yup");
const { Organisations } = require("./organisations.class");
const hooks = require("./organisations.hooks");
const schema = require("./organisations.yup");

module.exports = (app, optionsOverride) => {
  const options = {
    id: "alias",
    paginate: app.get("paginate"),
    schema: mergeValidationsSchema(schema, optionsOverride.schema),
  };

  // Initialize our service with any options it requires
  app.use("/organisations", new Organisations(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("organisations");

  service.hooks(hooks);
};
