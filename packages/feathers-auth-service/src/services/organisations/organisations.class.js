const { Service } = require("feathers-mongodb");
const { onPluginReady } = require("@lpgroup/feathers-plugins");

class Organisations extends Service {
  constructor(options) {
    super(options);

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("organisations");
      database.createIndexThrowError("organisations", { alias: 1 }, { unique: true });
    });
  }
}

module.exports = { Organisations };
