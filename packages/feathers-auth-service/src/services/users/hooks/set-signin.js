const { v4: uuidv4 } = require("uuid");

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    const { data } = context;
    if (data.type === "user") {
      data.signInCode = uuidv4();
      data.signInEnabled = true;
    } else {
      data.signInCode = null;
      data.signInEnabled = false;
    }

    return context;
  };
};
