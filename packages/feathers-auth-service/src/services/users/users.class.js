const { Service } = require("feathers-mongodb");
const { onPluginReady } = require("@lpgroup/feathers-plugins");

class Users extends Service {
  constructor(options) {
    super(options);
    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("users");
      database.createIndexThrowError("users", { email: 1 }, { unique: true });
    });
  }
}

module.exports = { Users };
