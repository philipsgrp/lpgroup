const cy = require("@lpgroup/yup");

// TODO: Måste validera olika beroende på type.

const requestSchema = {
  _id: cy.id(),
  email: cy.email().defaultNull(),
  password: cy.string().defaultNull(),
  firstName: cy.string().defaultNull(),
  lastName: cy.string().defaultNull(),
  phone: cy.phone().defaultNull(),

  // TODO: Får ej ändras via external
  privileges: cy.arrayObject({
    _id: cy.id(),
    alias: cy.alias(),
    params: cy.object({
      userId: cy.uuid().defaultNull(),
      organisationAlias: cy.uuid().defaultNull(),
    }),
  }),
  type: cy
    .labelText()
    .matches(/(user|device)/)
    .default("device"),
};

const dbSchema = {
  active: cy.boolean().default(true),
  signInCode: cy.string().defaultNull(),
  signInEnabled: cy.boolean().default(false),
  added: cy.changed(),
  changed: cy.changed(),
  owner: cy.userOwner(),
};
module.exports = cy.buildValidationSchema(requestSchema, dbSchema);
