const debug = require("debug")("auth");
const { hashPassword: hash } = require("@feathersjs/authentication-local").hooks;
const { discard, iff, isProvider } = require("feathers-hooks-common");
const { authenticate } = require("@feathersjs/authentication").hooks;
const { patchData, loadData } = require("@lpgroup/feathers-mongodb-hooks").hooks;
const { validReq, validDB } = require("@lpgroup/yup");
const { url } = require("@lpgroup/feathers-utils").hooks;
const changed = require("../../hooks/changed");
const checkPermissions = require("../../hooks/check-permissions");
const owner = require("./hooks/set-owner-user");
const signIn = require("./hooks/set-signin");

const debugLoginError = (context) => {
  debug(`Invalid user/JWT ${context.id} ${context.error.message}`);
};

module.exports = {
  before: {
    // Don't check permissions for internal calls.
    all: [authenticate("jwt"), iff(isProvider("external"), checkPermissions())],
    find: [],
    get: [],
    create: [hash("password"), validReq(), owner(), signIn(), changed(), validDB()],
    update: [hash("password"), validReq(), loadData(), owner(), signIn(), changed(), validDB()],
    patch: [hash("password"), validReq(), patchData(), owner(), signIn(), changed(), validDB()],
    remove: [],
  },

  after: {
    // Make sure the password field is never sent to the client
    // Always must be the last hook
    all: [
      url({ key: "alias" }),
      iff(isProvider("external"), discard("password", "privileges", "signInCode", "signInEnabled")),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [debugLoginError],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
