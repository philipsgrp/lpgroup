// Initializes the `users` service on path `/users`
const { mergeValidationsSchema } = require("@lpgroup/yup");
const { Users } = require("./users.class");
const hooks = require("./users.hooks");
const schema = require("./users.yup");

module.exports = (app, optionsOverride) => {
  const options = {
    id: "_id",
    paginate: app.get("paginate"),
    schema: mergeValidationsSchema(schema, optionsOverride.schema),
  };

  // Initialize our service with any options it requires
  if (optionsOverride.Users) {
    app.use("/users", new optionsOverride.Users(options, app));
  } else {
    app.use("/users", new Users(options, app));
  }

  // Get our initialized service so that we can register hooks
  const service = app.service("users");

  service.hooks(hooks);
};

module.exports.Users = Users;
