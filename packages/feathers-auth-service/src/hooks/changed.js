/* eslint-disable no-param-reassign */
const { buildItemHook } = require("@lpgroup/feathers-utils");

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return buildItemHook((context, item) => {
    const superUser = context.params.superUser ? { _id: "superuser" } : undefined;

    const user = superUser || item.user || context.params.user;
    if (user) {
      item.changed = { by: user._id, at: Date.now() };

      // Only set added when creating an item
      if (!item.added) item.added = item.changed;

      // Owner can be modified by other service hooks.
      // They are responsible for not overwriting the user._id
      if (!("owner" in item)) item.owner = {};
      if (!("user" in item.owner)) item.owner.user = { _id: user._id };
    }
  });
};
