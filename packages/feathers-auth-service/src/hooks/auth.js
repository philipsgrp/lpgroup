const { combine } = require("feathers-hooks-common");
const { authenticate } = require("@feathersjs/authentication").hooks;
const checkPermissions = require("./check-permissions");

// eslint-disable-next-line no-unused-vars
module.exports = (extraPrivilege) => {
  return (context) => {
    // Only validate auth token if it exist. Otherwise let checkPermission return
    // error message, if no public privilegies exist.
    if (context.params.authentication)
      return combine(authenticate("jwt"), checkPermissions(extraPrivilege)).call(this, context);
    return checkPermissions(extraPrivilege)(context);
  };
};
