module.exports = () => {
  return async (context) => {
    if (context.params.provider) context.params.provider = "";
    return context;
  };
};
