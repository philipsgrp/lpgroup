const auth = require("./hooks/auth");
const changed = require("./hooks/changed");
const checkPermissions = require("./hooks/check-permissions");
const authentication = require("./services/authentication/authentication.service.js");
const users = require("./services/users/users.service.js");
const usersUpgrade = require("./services/users-upgrade/users-upgrade.service.js");
const privileges = require("./services/privileges/privileges.service.js");
const organisations = require("./services/organisations/organisations.service.js");

module.exports = {
  hooks: { auth, changed, checkPermissions },
  authentication,
  users,
  usersUpgrade,
  privileges,
  organisations,
};
