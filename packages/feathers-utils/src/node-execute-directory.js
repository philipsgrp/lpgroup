/* eslint-disable no-await-in-loop */
/* eslint-disable import/no-dynamic-require, global-require, no-console */
const fs = require("fs");
const { join } = require("path");

const defaultOptions = {
  verbose: false,
  extensions: ["js", "json"],
  recurse: true,
  rename(name) {
    return name;
  },
  visit(obj) {
    return obj;
  },
};

/**
 * Filter (include, exclude) files that should be imported.
 *
 * Return True if file should be imported/included.
 */
function generateWhiteList(extension, include, exclude) {
  return (path) => {
    const reExtension = RegExp(extension);
    const reInclude = RegExp(include);
    const reExclude = RegExp(exclude);
    return (
      !/config.js/.test(path) &&
      !/\/src\//.test(path) &&
      reExtension.test(path) &&
      reInclude.test(path) &&
      (!exclude || !reExclude.test(path))
    );
  };
}

function checkFileInclusion(path, filename, options) {
  return (
    // verify file has valid extension
    new RegExp(`\\.(${options.extensions.join("|")})$`, "i").test(filename) &&
    // if options.include is a RegExp, evaluate it and make sure the path passes
    !(options.include && options.include instanceof RegExp && !options.include.test(path)) &&
    // if options.include is a function, evaluate it and make sure the path passes
    !(
      options.include &&
      typeof options.include === "function" &&
      !options.include(path, filename)
    ) &&
    // if options.exclude is a RegExp, evaluate it and make sure the path doesn't pass
    !(options.exclude && options.exclude instanceof RegExp && options.exclude.test(path)) &&
    // if options.exclude is a function, evaluate it and make sure the path doesn't pass
    !(options.exclude && typeof options.exclude === "function" && options.exclude(path, filename))
  );
}

function readdir(path, options) {
  const directories = [];
  const files = [];

  fs.readdirSync(path).forEach((filename) => {
    const joined = join(path, filename);
    if (fs.statSync(joined).isDirectory()) directories.push(joined);
    if (checkFileInclusion(joined, filename, options)) files.push(joined);
  });
  directories.sort();
  files.sort();
  return { directories, files };
}

async function executeDirectory(path, optionsOveride = {}) {
  const options = { ...defaultOptions, ...optionsOveride };

  // Get all files and directories in path, not recursively
  const { directories, files } = readdir(path, options);

  // First execute all files synchronous in the folder
  let executedFiles = [];
  for (let i = 0; i < files.length; i += 1) {
    const fileName = files[i];
    if (options.verbose) console.log(`File: ${fileName}`);
    const obj = require(fileName);
    await obj().catch((err) => console.error(err.message));

    executedFiles.push(fileName);
  }
  // Second execute all subdirectories asynchronous
  if (options.recurse) {
    const executedDirectoreis = await Promise.all(
      directories.map((directory) => executeDirectory(directory, optionsOveride, executedFiles))
    );
    executedFiles = [...executedFiles, ...executedDirectoreis.flat()];
  }
  return executedFiles;
}

module.exports = { executeDirectory, generateWhiteList };
