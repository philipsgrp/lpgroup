/* eslint-disable no-underscore-dangle */
function isObject(obj) {
  return obj && typeof obj === "object";
}

function hashObject(obj) {
  if (Array.isArray(obj)) {
    return JSON.stringify(obj);
  }
  if (isObject(obj)) {
    if (obj.alias) return obj.alias;
    if (obj._id) return obj._id;
    return JSON.stringify(
      Object.keys(obj)
        .sort()
        .filter((key) => {
          return !Array.isArray(obj[key]) && !isObject(obj[key]);
        })
        .map((key) => {
          return [key, obj[key]];
        })
    );
  }
  return obj;
}

function compareArrayObjects(a, b) {
  const aKey = hashObject(a);
  const bKey = hashObject(b);

  // eslint-disable-next-line no-nested-ternary
  return aKey < bKey ? -1 : aKey > bKey ? 1 : 0;
}

function deepSortObjectArray(root) {
  if (Array.isArray(root)) {
    return root.map(deepSortObjectArray).sort(compareArrayObjects);
  }

  if (isObject(root)) {
    return Object.keys(root)
      .sort()
      .reduce((sortedRoot, key) => {
        // eslint-disable-next-line no-param-reassign
        sortedRoot[key] = deepSortObjectArray(root[key]);
        return sortedRoot;
      }, {});
  }

  return root;
}

module.exports = { deepSortObjectArray };
