/**
 * A feathersjs hook that can be used to debug other hooks.
 *
 * Add this before or after other hooks to get current state of context
 * or just a message. Uses npm debug to write messages.
 */
const debug = require("debug")("debug");

module.exports = (message = null, fullPrint = false) => {
  return async (context) => {
    let all = `HOOK: ${context.method} - ${context.path} - `;
    if (message) all += message;
    debug(all);
    if (fullPrint) debug(context);
    return context;
  };
};
