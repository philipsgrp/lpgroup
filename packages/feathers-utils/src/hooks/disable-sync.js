const { SYNC } = require("feathers-sync");

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    context[SYNC] = false;
  };
};
