/**
 * feathersjs supports both rest and websockets. Websockets
 * uses query-parameters instead of routing parameters. We decided
 * to only use query parameters everywhere. This code will MOVE
 * routing parameters to the query parameter.
 */
// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  // TODO: Maybe we should user $populate, looks like feathersjs standard?
  const whiteList = ["populate", "unpopulate"];
  return async (context) => {
    const { params } = context;

    // Move whiteListed query parameters to params
    Object.keys(params.query || {}).forEach((query) => {
      if (whiteList.includes(query)) {
        params[query] = params.query[query];
        delete params.query[query];
      }
    });

    Object.keys(params.route || {}).forEach((param) => {
      params.query[param] = params.route[param];
      delete params.route[param];
    });
    return context;
  };
};
