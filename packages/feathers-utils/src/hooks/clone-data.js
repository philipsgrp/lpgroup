// Clone context.data, it will be used by validate-changes
// to verify that nothing has been changed.
//

const { cloneDeep } = require("lodash");

// eslint-disable-next-line no-unused-vars
module.exports = (options = { name: "clone" }) => {
  return async (context) => {
    context.custom = context.custom || {};
    context.custom[options.name] = cloneDeep(context.data);
    return context;
  };
};
