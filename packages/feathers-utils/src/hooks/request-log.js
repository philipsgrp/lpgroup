const dbgRequest = require("debug")("req.ext");
const debInternal = require("debug")("req.srv");
const dbgException = require("debug")("exception");

// All external providers that can exist in params.provider.
const providers = ["rest", "socketio", "primus"];

const ignoreUrls = ["ready", "healthy"];
function getUrl(context) {
  let url = Object.entries(context.params.query || {}).reduce(
    (accUrl, { key, value }) => accUrl.replace(`:${key}`, value),
    context.path
  );

  if (context.id) url += `/${context.id}`;

  if (ignoreUrls.includes(url)) return "";
  return url.charAt(0) === "/" ? url : `/${url}`;
}

module.exports = () => {
  return (context) => {
    if (context.type === "before") {
      const url = getUrl(context);
      if (url) {
        if (providers.includes(context.params.provider)) dbgRequest(`${context.method} ${url}`);
        else debInternal(`${context.method} ${url}`);
      }
    }

    if (context.error) {
      if (context.error.className === "TypeError") {
        dbgException(context.error.stack);
      } else if (!context.result && context.error.type !== "FeathersError") {
        if (
          context.error.message === "Current topology does not support sessions" ||
          context.error.codeName === "NoSuchTransaction"
        ) {
          dbgException(context.error.message);
        } else {
          dbgException(context.error.stack);
        }
      }
    }
  };
};
