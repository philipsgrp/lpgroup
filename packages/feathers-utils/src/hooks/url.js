/* eslint-disable no-param-reassign */
/**
 * Add url key to json row.
 */
// eslint-disable-next-line no-unused-vars
module.exports = (options = { key: "_id" }) => {
  return async (context) => {
    // eslint-disable-next-line no-underscore-dangle
    if (context.params._calledByHook) return context;

    const { app, result } = context;
    const host = app.get("mainDomain");

    const url = Object.entries(context.params.query || {}).reduce(
      (accUrl, { key, value }) => accUrl.replace(`:${key}`, value),
      context.path
    );

    // Is it used by find or get
    const localResult = result.data ? result.data : [result];
    localResult.forEach((data) => {
      data.url = host.concat("/", url);
      if (options.key) data.url += `/${data[options.key]}`;
    });

    return context;
  };
};
