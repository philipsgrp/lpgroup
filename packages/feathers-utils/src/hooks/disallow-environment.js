/**
 * If a system environment variable is set to a specific value, throw 404 error.
 *
 * Example
 * const { disallowEnvironment } = require('@lpgroup/feathers-utils')
 * module.exports = {
 *  before: {
 *    all: [disallowEnvironment({ NODE_CONFIG_ENV: "prod" })],
 *  }
 * };
 *
 */
const { NotFound } = require("@feathersjs/errors");

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    Object.entries(options).forEach(([env, value]) => {
      if (process.env[env] && process.env[env].toLowerCase() === value.toLowerCase())
        throw new NotFound();
    });
    return context;
  };
};
