const { getItems } = require("feathers-hooks-common");

function buildItemHook(cb) {
  return async (context) => {
    const items = getItems(context);
    const arrItems = Array.isArray(items) ? items : [items];

    await Promise.all(
      arrItems.map(async (item) => {
        return cb(context, item);
      })
    );

    return context;
  };
}

/**
 * Create a fethersjs params object to use on internal calls.
 */
function internalParams(params, query = {}, options = {}) {
  const newParam = { ...params, query, provider: "" };
  if (options.clearSession) {
    if (newParam.mongodb) delete newParam.mongodb;
    if (newParam.sessionId) delete newParam.sessionId;
  }
  return newParam;
}

module.exports = { buildItemHook, internalParams };
