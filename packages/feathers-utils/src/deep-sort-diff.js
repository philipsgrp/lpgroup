const { diff } = require("deep-diff");
const { deepSortObjectArray } = require("./deep-sort-object-array");

function deepSortDiff(a, b) {
  const sortedA = deepSortObjectArray(a);
  const sortedB = deepSortObjectArray(b);

  const result = diff(sortedA, sortedB);
  return result || [];
}

module.exports = { deepSortDiff };
