// Hooks
const cloneData = require("./hooks/clone-data");
const debugHook = require("./hooks/debug-hook");
const disableSync = require("./hooks/disable-sync");
const disallowEnvironment = require("./hooks/disallow-environment");
const requestLog = require("./hooks/request-log");
const setQuery = require("./hooks/set-query");
const url = require("./hooks/url");

// Middleware
const protocol = require("./middleware/protocol");

// Common utils
const axiosCommon = require("./axios");
const common = require("./common");
const compare = require("./compare");
const deepSortDiff = require("./deep-sort-diff");
const deepSortObjectArray = require("./deep-sort-object-array");
const mergeData = require("./merge-data");
const nodeExecuteDirectory = require("./node-execute-directory");

module.exports = {
  hooks: {
    cloneData,
    debugHook,
    disableSync,
    disallowEnvironment,
    requestLog,
    setQuery,
    url,
  },
  protocol,
  ...axiosCommon,
  ...common,
  ...compare,
  ...deepSortDiff,
  ...deepSortObjectArray,
  ...mergeData,
  ...nodeExecuteDirectory,
};
