const { deepSortDiff } = require("./deep-sort-diff");

function removeEditedKeys(diff, ignoreKeyCompare) {
  if (diff === undefined) return [];
  return diff.filter((row) => {
    if ((row.kind === "E" || row.kind === "D") && row.path) {
      // If any of the json keys in the row.path array are to be
      // ignored. The filter shouldn't return them.
      if (row.path.some((v) => ignoreKeyCompare.includes(v))) return false;
    }
    return true;
  });
}

/**
 *
 * @param {*} response
 * @param {*} options {
 *   expected: {},
 *   ignoreKeCompare: ["key"]
 * }
 */
function checkExpected(response, options) {
  if ("expected" in options) {
    const { ignoreKeyCompare = [] } = options;
    // TODO: Fungerar utan denna?
    // if (!requiredKeys.every((k) => k in response)) {
    //   debug(`Response need the following keys ${requiredKeys}`);
    // }

    const diff = removeEditedKeys(deepSortDiff(response, options.expected), ignoreKeyCompare);
    if (diff.length > 0) {
      if (!options.waitUntilExpected) {
        console.error("Response: ");
        console.error(JSON.stringify(response, null, "  "));
        console.error("Diff: ");
        console.error(JSON.stringify(diff, null, "  "));
      }
      return true;
    }
  }
  return false;
}

module.exports = { checkExpected };
