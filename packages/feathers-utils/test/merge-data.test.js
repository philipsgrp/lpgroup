const { cloneDeep } = require("lodash");
const { mergeData } = require("../src/merge-data");
const { deepSortObjectArray } = require("../src/deep-sort-object-array");

describe("utils/merge-data", () => {
  const source = {
    _id: "ae270dc6-1410-4007-811a-40054f07aa2b",
    alias: "flode-for-mat",
    title: "Flöde för mat",
    description: "Flöder för mat",
    actions: [
      {
        alias: "created",
        type: "system",
        title: "Beställd",
        workerGroups: [
          {
            alias: "kok",
          },
        ],
      },
      {
        alias: "paid",
        title: "Betalad",
        type: "system",
        workerGroups: [
          {
            alias: "servitor-1",
          },
          {
            alias: "servitor-2",
          },
        ],
      },
      {
        alias: "canceled",
        title: "Avbeställd",
        type: "system",
        workerGroups: [
          {
            alias: "servitor-1",
          },
          {
            alias: "servitor-2",
          },
        ],
      },
    ],
  };

  test("equal arrays", async () => {
    const source1 = cloneDeep(source);
    const source2 = cloneDeep(source);

    const sourceTarget = mergeData(source1, source2);
    expect(sourceTarget).toEqual(source);
  });

  test("one extra key", async () => {
    const source1 = cloneDeep(source);
    const source2 = cloneDeep(source);
    source1.actions.splice(1, 1);

    const sourceTarget = mergeData(source1, source2);

    const sortedSourceTarget = deepSortObjectArray(sourceTarget);
    const sortedSource = deepSortObjectArray(source);
    expect(sortedSourceTarget).toEqual(sortedSource);
  });

  test("modified key", async () => {
    const source1 = cloneDeep(source);
    const source2 = cloneDeep(source);
    source1.actions[0].title = "Mooo";

    const sourceTarget = mergeData(source1, source2);
    expect(sourceTarget).toEqual(source);
  });
});
