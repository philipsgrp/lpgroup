const path = require("path");
const { executeDirectory } = require("../src/node-execute-directory");

describe("node-execute-directory", () => {
  test("Golden path", async () => {
    const fullPath = path.resolve(`${__dirname}/../data/ned`);
    const result = (await executeDirectory(fullPath)).map((v) => v.replace(fullPath, ""));
    expect(result).toEqual([
      "/01-data.js",
      "/02-data.js",
      "/03-data/03-01-data.js",
      "/03-data/03-02-data.js",
      "/03-data/03-data/03-03-01-data.js",
      "/03-data/03-data/03-03-02-data.js",
      "/03-data/04-data/03-04-01-data.js",
      "/03-data/04-data/03-04-02-data.js",
      "/04-data/04-01-data.js",
      "/04-data/04-02-data.js",
      "/04-data/03-data/04-03-01-data.js",
      "/04-data/03-data/04-03-02-data.js",
      "/04-data/04-data/04-04-01-data.js",
      "/04-data/04-data/04-04-02-data.js",
    ]);
  });
});
