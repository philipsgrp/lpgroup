# @lpgroup/feathers-k8s-probe-services

[![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-k8s-probe-services.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-k8s-probe-services) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-k8s-probe-services/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-k8s-probe-services)
[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

Kubernetes (k8s) probe services for Feathers exposed on /healthy and /ready

They will both return 404 and the text specified in the config file.
Will start to send 200 ok after `app.set("status").ready = "OK"` is
executed

## Install

Installation of the npm

```sh
npm install @lpgroup/feathers-k8s-probe-services
```

## Usage

In `config/default.json` add the following.

```json
{
  "status": {
    "ready": "NOT-STARTED"
  }
}
```

In `src/servies/index.js`

```javascript
const { ready, healthy } = require("@lpgroup/feathers-k8s-probe-services");

app.configure(ready);
app.configure(healthy);
```

In `src/index.js`

```javascript
server.on("listening", async () => {
  app.set("status").ready = "OK";
});
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
