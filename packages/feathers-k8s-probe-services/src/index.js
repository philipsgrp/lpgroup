const healthy = require("./services/healthy/healthy.service.js");
const ready = require("./services/ready/ready.service.js");

module.exports = {
  healthy,
  ready,
};
