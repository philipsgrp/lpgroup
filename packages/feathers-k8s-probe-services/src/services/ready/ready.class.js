const { NotFound } = require("@feathersjs/errors");

/* eslint-disable no-unused-vars */
exports.Ready = class Ready {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find(params) {
    if (this.app.get("status").ready === "OK") return { code: 200, status: "OK" };
    throw new NotFound(`Not ready. Code: ${this.app.get("status").ready}`);
  }
};
