// Initializes the `healthy` service on path `/healthy`
const { Healthy } = require("./healthy.class");
const hooks = require("./healthy.hooks");

module.exports = (app) => {
  const options = {
    paginate: app.get("paginate"),
  };

  // Initialize our service with any options it requires
  app.use("/healthy", new Healthy(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("healthy");

  service.hooks(hooks);
};
