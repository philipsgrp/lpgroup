const { validate } = require("./utils");

// TODO: Can probably remove options.schema
function validReq(options = {}) {
  return async (context) => {
    if (context.method === "create" || context.method === "update") {
      context.data = await validate(
        context.data,
        (options.schema && options.schema.request) || context.service.options.schema.request
      );
    } else if (context.method === "patch") {
      // TODO: Check that only keys from requestSchema
      // are in the request json.
      // eslint-disable-next-line
    }
    return context;
  };
}

// eslint-disable-next-line no-unused-vars
function validDB(options = {}) {
  return async (context) => {
    context.data = await validate(context.data, context.service.options.schema.db);
    return context;
  };
}

// TODO: Obsolete validateRequest and validateDatabase
module.exports = { validateRequest: validReq, validateDatabase: validDB, validReq, validDB };
