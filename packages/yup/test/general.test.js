/* eslint-disable jest/expect-expect */
const { validate } = require("./utils");
const yup = require("../src/index");

describe("General", () => {
  describe("String", () => {
    test("valid", async () => {
      const schema = { key: yup.string() };
      validate({ key: "value" }, schema).resolves.toStrictEqual({ key: "value" });
      validate({ key: "value  " }, schema).resolves.toStrictEqual({ key: "value" });
      validate({ key: 23 }, schema).resolves.toStrictEqual({ key: "23" });
      validate({ key: "" }, schema).resolves.toStrictEqual({ key: "" });
      validate({}, schema).resolves.toStrictEqual({});
    });

    test("valid defaultNull", async () => {
      const schema = { key: yup.string().defaultNull() };
      validate({ key: null }, schema).resolves.toStrictEqual({ key: null });
      validate({}, schema).resolves.toStrictEqual({ key: null });
    });

    test("valid default", async () => {
      const schema = { key: yup.string().default("123") };
      validate({}, schema).resolves.toStrictEqual({ key: "123" });
    });

    test("invalid required", async () => {
      const schema = { key: yup.string().required() };
      validate({}, schema).rejects.toThrow("key is a required field");
    });

    test("invalid", async () => {
      const schema = { key: yup.string() };
      const msg =
        "key must be a `string` type, but the final value was: `null`.\n " +
        'If "null" is intended as an empty value be sure to mark the schema as `.nullable()`';
      validate({ key: null }, schema).rejects.toThrow(msg);
    });

    test("invalid object transform", async () => {
      const schema = { key: yup.string() };
      await validate({ key: { key2: "value" } }, schema).rejects.toThrow(
        'Value need to be of type string, {"key2":"value"}'
      );
      await validate({ key: ["13", "37"] }, schema).rejects.toThrow(
        'Value need to be of type string, ["13","37"]'
      );
    });
  });
});
