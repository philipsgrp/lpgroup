const debug = require("debug")("ghostscript");
const execa = require("execa");

async function streamToBuffer(stream) {
  return new Promise((resolve) => {
    const bufs = [];
    stream.on("data", (d) => {
      bufs.push(d);
    });
    stream.on("end", () => {
      const buf = Buffer.concat(bufs);
      resolve(buf);
    });
  });
}

async function optimize(opt) {
  const command = "gs";
  const { input } = opt;

  const {
    compatibilityLevel = 1.5,
    compressFonts = true,
    embedAllFonts = true,
    subsetFonts = true,
    dpi = 300,
    preset = "screen",
    colorConversionStrategy = "RGB",
  } = opt;

  const args = [
    "-sDEVICE=pdfwrite",
    `-dPDFSETTINGS=/${preset}`,
    "-dNOPAUSE",
    "-dQUIET",
    "-dBATCH",
    `-dCompatibilityLevel=${String(compatibilityLevel)}`,
    // font settings
    `-dSubsetFonts=${subsetFonts}`,
    `-dCompressFonts=${compressFonts}`,
    `-dEmbedAllFonts=${embedAllFonts}`,
    // color format
    "-sProcessColorModel=DeviceRGB",
    `-sColorConversionStrategy=${colorConversionStrategy}`,
    `-sColorConversionStrategyForImages=${colorConversionStrategy}`,
    "-dConvertCMYKImagesToRGB=true",
    // image resampling
    "-dDetectDuplicateImages=true",
    "-dColorImageDownsampleType=/Bicubic",
    `-dColorImageResolution=${dpi}`,
    "-dGrayImageDownsampleType=/Bicubic",
    `-dGrayImageResolution=${dpi}`,
    "-dMonoImageDownsampleType=/Bicubic",
    `-dMonoImageResolution=${dpi}`,
    "-dDownsampleColorImages=true",
    // other overrides
    "-dDoThumbnails=false",
    "-dCreateJobTicket=false",
    "-dPreserveEPSInfo=false",
    "-dPreserveOPIComments=false",
    "-dPreserveOverprintSettings=false",
    "-dUCRandBGInfo=/Remove",
    "-sOutputFile=-",
    "-",
  ].filter(Boolean);

  debug(`${command} ${args.join(" ")}`);
  let subprocess;
  try {
    subprocess = execa(command, args);

    subprocess.stdin.write(input);
    subprocess.stdin.end();

    const result = await streamToBuffer(subprocess.stdout);
    await subprocess;
    return result;
  } catch (err) {
    debug(err);
    throw new Error(`Can't optimize pdf ${err.stderr}`);
  }
}

module.exports = { optimize };
