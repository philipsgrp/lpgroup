const fs = require("fs").promises;
const { encode } = require("js-base64");
const path = require("path");
const { optimize } = require("../src/index");

describe("Ghostscript", () => {
  let input;
  const fileIn = path.resolve(`${__dirname}/flytt_uppdrag.pdf`);
  const fileOut = path.resolve(`${__dirname}/flytt_uppdrag_optimized.pdf`);

  beforeAll(async () => {
    input = await fs.readFile(fileIn);
  });

  beforeEach(async () => {
    try {
      await fs.unlink(fileOut);
      // eslint-disable-next-line no-empty
    } catch (err) {}
  });

  test("Optimize golden", async () => {
    const output = await optimize({ input });
    await fs.writeFile(fileOut, output, "binary");
    expect(output.length).toBeGreaterThan(45000);
    expect(output.length).toBeLessThan(60000);
  });

  test("Optimize error 1", async () => {
    const output = await optimize({ input });
    const base64 = `data:application/pdf;base64,${encode(output)}`;
    await expect(optimize({ input: base64 })).rejects.toThrow(Error);
  });

  // TODO: This test fails.
  // eslint-disable-next-line jest/no-commented-out-tests
  // test("Optimize error 2", async () => {
  //   return expect(optimize({ input: "asdfasdfasdfasdf" })).rejects.toEqual(
  //     new Error("Can't optimize pdf GPL Ghostscript 9.52: Unrecoverable error, exit code 1")
  //   );
  // });
});
