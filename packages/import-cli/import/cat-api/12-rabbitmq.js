// Projects using import-cli should have this dependency
// const { rabbitmq } = require("@lpgroup/import-cli");
const { rabbitmq } = require("../../src");

module.exports = async () => {
  return rabbitmq().then(async (rmq) => {
    // nc.subExpect("testing-nats", {
    //   expected: { name: "8615d9d0-e7c5-4f26-90f3-a32ef424cba3" }
    // });

    await rmq.sentToQueue("lpgroup", "The Good Cow");

    // await nc.waitForSubExpect("testing-nats");
  });
};
