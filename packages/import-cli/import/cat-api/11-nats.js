// Projects using import-cli should have this dependency
// const { nats } = require("@lpgroup/import-cli");
const { nats } = require("../../src");

module.exports = async () => {
  await nats().then(async (nc) => {
    nc.subExpect("testing-nats", {
      expected: { name: "8615d9d0-e7c5-4f26-90f3-a32ef424cba3" },
    });

    await nc.pub("testing-nats", {
      name: "8615d9d0-e7c5-4f26-90f3-a32ef424cba3",
    });

    await nc.waitForSubExpect("testing-nats");
  });
};
