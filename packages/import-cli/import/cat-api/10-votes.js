// Projects using import-cli should have this dependency
// const { axios } = require("@lpgroup/import-cli");
const { axios } = require("../../src");

module.exports = async () => {
  return axios().then(async (ax) => {
    await ax.post("/votes", {
      image_id: "asf2",
      sub_id: "my-user-1234",
      value: 1,
    });
  });
};
