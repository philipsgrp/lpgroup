const {
  addPluginWithOptions,
  closePlugins,
  onPluginsReady,
  onPluginReady,
  axios,
  nats,
  rabbitmq,
} = require("@lpgroup/feathers-plugins");

function setupPlugins(config) {
  if (config.http.server)
    addPluginWithOptions("axios", axios, {
      baseURL: config.http.server,
      customHeader: "import-cli",
      errorHandlerWithException: false,
      requiredKeys: config.requiredKeys,
      ignoreKeyCompare: config.ignoreKeyCompare,
      user: config.http.user,
      password: config.http.password,
      readyServer: config.http.readyServer,
      headers: config.http.headers,
      maxRPS: 200,
    });

  if (config.nats.uri)
    addPluginWithOptions("nats", nats, {
      name: "import-cli",
      uri: config.nats.uri,
      queue: config.nats.queue,
      messagePrefix: config.nats.messagePrefix,
      ignoreKeyCompare: config.ignoreKeyCompare,
    });

  if (config.rabbitmq.uri)
    addPluginWithOptions("rabbitmq", rabbitmq, {
      uri: config.rabbitmq.uri,
      queue: config.rabbitmq.queue,
    });

  return onPluginsReady();
}

module.exports = {
  setupPlugins,
  closePlugins,
  axios: async () => onPluginReady("axios"),
  nats: async () => onPluginReady("nats"),
  rabbitmq: async () => onPluginReady("rabbitmq"),
};
