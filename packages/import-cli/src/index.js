/**
 * Helper functions that can be used by the import scripts
 */
const { PDFDocument } = require("pdf-lib");
const fs = require("fs").promises;
const path = require("path");
const { axios, nats, rabbitmq } = require("./plugins");

/**
 * Load a pdf-file into a base64 uri string.
 */
async function loadPdf(fileName) {
  const fullPath = path.resolve(fileName);
  const file = await fs.readFile(fullPath);
  const pdf = await PDFDocument.load(file);
  const uri = await pdf.saveAsBase64({ dataUri: true });
  return uri;
}

module.exports = { loadPdf, axios, nats, rabbitmq };
