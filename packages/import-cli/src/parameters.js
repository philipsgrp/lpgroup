/* eslint-disable no-console */
const { program } = require("commander");
const path = require("path");
const fs = require("fs");
const Table = require("table-layout");

function getCmdLineArgs() {
  program
    .version("0.0.1", "--version", "output the current version")

    .option("-e, --environment <env>", "Environment from .import.json to use", "default")
    .option("-w, --cwd <folder>", "Folder to import from", "./import")
    .option("-t, --extension <reqexp>", "Regexp of file extension to include", ".js$")
    .option("-i, --include <reqexp>", "Regexp to files to only include", "")
    .option("-x, --exclude <reqexp>", "Regexp to files to exclude", "")
    .option("-v, --verbose", "Regexp to files to exclude", false)
    .parse(process.argv);

  program.opts().cwd = path.resolve(program.opts().cwd);
  if (!fs.existsSync(program.opts().cwd)) {
    console.error(`Folder doesn't exist: ${program.opts().cwd}`);
  }
  return { ...program.opts(), version: program.version() };
}

function getConfig(env) {
  let config = {};
  const filePath = path.resolve(".import.json");
  if (fs.existsSync(filePath)) {
    // eslint-disable-next-line global-require, import/no-dynamic-require
    config = require(filePath);
  }

  if (config.environments && config.environments[env]) {
    const confEnv = config.environments[env];
    const { server, user, password, readyServer, headers = {} } = confEnv.http || {};
    const { uri, queue, messagePrefix } = confEnv.nats || {};
    const { uri: rabbitUri, queue: rabbitQueue } = confEnv.rabbitmq || {};

    return {
      http: { server, user, password, readyServer, headers },
      nats: { uri, queue, messagePrefix },
      rabbitmq: { uri: rabbitUri, queue: rabbitQueue },
      requiredKeys: config.requiredKeys || {},
      ignoreKeyCompare: config.ignoreKeyCompare || {},
    };
  }
  throw Error("Environment doesn't exist in .import.json");
}

function printParameters(parameters, config) {
  console.log(`import-cli (${parameters.version})`);
  const table = new Table(
    [
      { name: "Environment:", value: parameters.env },
      { name: "CWD:", value: parameters.cwd },
      { name: "Extension:", value: parameters.extension },
      { name: "Include:", value: parameters.include || "NA" },
      { name: "Exclude:", value: parameters.exclude || "NA" },
      { name: "Verbose:", value: parameters.verbose },

      { name: "requiredKeys:", value: config.requiredKeys || "NA" },
      { name: "ignoreKeyCompare:", value: config.ignoreKeyCompare || "NA" },

      { name: "http.server:", value: config.http.server || "NA" },
      { name: "http.user:", value: config.http.user || "NA" },
      { name: "http.password:", value: "***" },
      { name: "http.readyServer:", value: config.http.readyServer || "NA" },
      { name: "http.headers:", value: JSON.stringify(config.http.headers) || "NA" },

      { name: "nats.uri:", value: config.nats.uri || "NA" },
      { name: "nats.queue:", value: config.nats.queue || "NA" },
      { name: "nats.messagePrefix:", value: config.nats.messagePrefix || "NA" },

      { name: "rabbitmq.uri:", value: config.rabbitmq.uri || "NA" },
      { name: "rabbitmq.queue:", value: config.rabbitmq.queue || "NA" },
    ],
    { maxWidth: 100 }
  );
  console.log(table.toString());
}

module.exports = { getCmdLineArgs, getConfig, printParameters };
