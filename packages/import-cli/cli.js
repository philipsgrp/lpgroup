#!/usr/bin/env node

// Needed by local packages installed with "npm link" to be able
// to find it's dependencies.
// TODO: Remove now when using lerna/yarn??
if (process.env.NODE_ENV !== "PRODUCTION") {
  // eslint-disable-next-line global-require
  const path = require("path");
  process.env.NODE_PATH = path.resolve("./node_modules");
  // eslint-disable-next-line
  require("module").Module._initPaths();
}

const debugSettings = require("debug");
const { executeDirectory, generateWhiteList } = require("@lpgroup/feathers-utils");
const { getCmdLineArgs, getConfig, printParameters } = require("./src/parameters");
const { setupPlugins, closePlugins } = require("./src/plugins");

function main() {
  const parameters = getCmdLineArgs();
  const config = getConfig(parameters.environment);
  console.time("Time");
  printParameters(parameters, config);

  if (parameters.verbose) {
    // Axios output to console.
    debugSettings.enable(`${debugSettings.load()},axios,axios:error,nats,rabbitmq,compare`);
    debugSettings.save();
  }

  // The script can handle one server of each type
  setupPlugins(config).then(() => {
    const options = {
      include: generateWhiteList(parameters.extension, parameters.include, parameters.exclude),
      verbose: parameters.verbose,
    };
    executeDirectory(parameters.cwd, options).then(() => {
      // eslint-disable-next-line no-console
      console.log("Done");
      console.timeEnd("Time");
      closePlugins();
    });
  });
}

try {
  main();
} catch (error) {
  console.error(error.message);
  console.error(error);
}
