const { endSessionAndCommitTransaction, debugMsg } = require("../sessions");

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => async (context) => {
  const { params } = context;

  if (!params.sessionId) {
    console.error("No session started", context);
    throw Error("No session started");
  }
  debugMsg("endSession", context);
  return endSessionAndCommitTransaction(context);
};
