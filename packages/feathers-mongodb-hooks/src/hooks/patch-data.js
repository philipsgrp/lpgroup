/* eslint-disable no-underscore-dangle */
const { mergeData } = require("@lpgroup/feathers-utils");
const { NotFound } = require("@feathersjs/errors");
const { startGetAndLockTransaction } = require("../sessions");

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => async (context) => {
  if (context.method === "patch") {
    const dbData = await startGetAndLockTransaction(context, options.collections);
    if (!dbData) {
      throw new NotFound(`No record found for id '${context.id}'`);
    }

    context.data = mergeData(dbData, context.data);
  }

  return context;
};
