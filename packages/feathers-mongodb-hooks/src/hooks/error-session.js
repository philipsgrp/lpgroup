const { endSession, debugMsg } = require("../sessions");

function shouldLogError(error) {
  // 405 method not allowed
  if ([405, 404].includes(error.code)) return false;
  // 11000 - some mongodb error
  // E11000 duplicate key error collection:
  if (error.data && [11000].includes(error.data.code)) return false;

  return true;
}

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => async (context) => {
  debugMsg("errorSession ", context, context.error.message);

  if (shouldLogError(context.error)) {
    console.error(
      "feathers-mongodb-hooks/error-session",
      context.path,
      "\n",
      context.error.message,
      "\n",
      context.error.inheritedStack || "",
      context.error.stack,
      JSON.stringify(context.data, null, "  ")
    );
  }

  await endSession(context.params);

  return context;
};
