const startSession = require("./hooks/start-session");
const endSession = require("./hooks/end-session");
const errorSession = require("./hooks/error-session");
const loadData = require("./hooks/load-data");
const patchData = require("./hooks/patch-data");
const lockData = require("./hooks/lock-data");
const sessions = require("./sessions");
const exceptions = require("./exceptions");

module.exports = {
  hooks: {
    startSession,
    endSession,
    errorSession,
    loadData,
    patchData,
    lockData,
  },
  sessions,
  exceptions,
};
